/**
 * Subset sum problem (SSP) solver.
 *
 * Example:
 * Inputs: multiset = [3, 5, 7]; target = 10.
 * Output: {set: [7, 5, 3], solutions: [[0, 2], [1, 1]]}
 * meaning that there are two possible solutions: 7+3 and 5+5.
 *
 * cf https://en.wikipedia.org/wiki/Subset_sum_problem
 *
 * @param {Array<Number>} multiset - vector of unique positive integers
 * @param {Number} target - positive integer
 * @returns {Object} - `{set, solutions}`, where `set` is the original `multiset` only sorted, and `solutions` is
 * a vector of vectors of indices
 */

const ssp = (multiset, target) => {

    const MAX = Number.POSITIVE_INFINITY; // 100;
    const SET = multiset.sort((a, b) => {
        if (a < b)
            return 1;
        else if (a > b)
            return -1;
        else
            return 0;
    });
    const N = SET.length;

    const sum = () => s.length > 0 ? s.map(x => SET[x]).reduce((prev, cur) => prev + cur) : NaN;

    let log = '';
    let i = 0;
    let all = [];
    let s = [0];
    let p = 1;
    let e = sum();

    while (i < MAX && p > 0) {
        log += `Iter ${i}: [${s.map(x => SET[x])}] (length ${p}) → ${e} {${all}}${target === e ? ' FOUND!' : ''}\n`;
        if (target === e) {
            all.push([...s]);
            if (s[p - 1] < N - 1) {
                s[p - 1] = s[p - 1] + 1;
                e = sum();
            } else {
                while (p > 0 && s[p - 1] === N - 1) {
                    s = s.slice(0, p - 1);
                    p --;
                }
                if (p > 0) {
                    s[p - 1] = s[p - 1] + 1;
                    e = sum();
                }
            }
        } else if (e > target) {
            if (s[p - 1] < N - 1) {
                s[p - 1] = s[p - 1] + 1;
                e = sum();
            } else {
                while (p > 0 && s[p - 1] === N - 1) {
                    s = s.slice(0, p - 1);
                    p --;
                }
                if (p > 0) {
                    s[p - 1] = s[p - 1] + 1;
                    e = sum();
                }
            }
        } else {
            s.push(s[p - 1]);
            p ++;
            e = sum();
        }
        i ++;
    }

    return {
        set: SET,
        solutions: all
    };
};

export default ssp;
