import { error, debug } from 'console';
import { basename } from 'path';
import { argv } from 'process';

import tk from 'terminal-kit';
import { Palette } from 'supers';

import ssp from './algo.mjs';

const PALETTE_OPTS = {
    blackAndWhite: false,
    greys: false
};
const TYPE_INT = 0;
// const TYPE_FLOAT = 1;
// const TYPE_SMPTE = 2;

const REGEX_SMPTE = /((\d+:){0,3}\d+[\ ,])*(\d+:){0,3}\d+/i;

const failWithMessage = msg => {
    process.exitCode = 1;
    error(msg);
};

const parseInput = input => {
    // let type = TYPE_FLOAT;
    // if (REGEX_SMPTE.test(input))
    //     type = TYPE_SMPTE;
    // return [type];
    const v = input.split(/[\ ,]+/).map(x => parseInt(x));
    return [TYPE_INT, v.length > 1 ? v : v[0]];
};

const count = v => {
    const result = [];
    let c = v[0];
    let n = 1;
    for (let i = 1; i < v.length; i ++) {
        if (v[i] === c)
            n ++;
        else {
            result.push({
                c,
                n
            });
            c = v[i];
            n = 1;
        }
    }
    result.push({
        c,
        n
    });
    return result;
};

if (4 !== argv.length)
    failWithMessage(
        'Wrong no. of params!\n' +
        `Expecting: ${basename(argv[1])} <MULTISET> <TARGET>\n` +
        `eg: ${basename(argv[1])} "18 3 9.2" 135.2\n`
    );
else {
    const s = parseInput(argv[2]);
    const target = parseInput(argv[3]);
    // if (s[0] !== set[1])...
    const {set, solutions} = ssp(s[1], target[1]);    
    if (solutions.length > 0) {
        const p = new Palette(set.length, PALETTE_OPTS);
        debug(`Solutions found (${solutions.length}):`);
        solutions.forEach((x, i) => {
            const processed = count(x);
            tk.terminal.defaultColor(`${i + 1}: [`);
            processed.forEach(y => {
                const {c, n} = y;
                tk.terminal.colorRgbHex('#' + p.get(c, true), set[c]).grey(`×${n}, `);
            });
            tk.terminal.backDelete().backDelete()(']\n');
        });
    }
}
