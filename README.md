# Subset sum problem (SSP) solver

![Example 1](img/example1.png)

![Example 2](img/example2.png)

## Requisites

* Node.js &ge; 14
* npm &ge; 7

## Syntaxis

```sh
node index.mjs <multiset> <target>
```

Where:

* `multiset` is an array of unique positive integers
* `target` is a positive integer

## Example

* Inputs: `multiset` &rarr; `[3, 5, 7]`; `target` &rarr; `10`
* Output: `{set: [7, 5, 3], solutions: [[0, 2], [1, 1]]}` (eaning: there are two possible solutions: `7 + 3` and `5 + 5`).

## More info

cf [the Wikipedia article](https://en.wikipedia.org/wiki/Subset_sum_problem)
